<?php
namespace PFBC\Element;

class Url extends Textbox {
	protected $_attributes = array("type" => "url", "class"=>"form-control");

	public function render() {
		$this->validation[] = new \PFBC\Validation\Url;
		parent::render();
	}
}
