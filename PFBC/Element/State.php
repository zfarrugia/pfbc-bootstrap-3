<?php
namespace PFBC\Element;

class State extends Select {
    protected $_attributes = array("class"=>"form-control");

	public function __construct($label, $name, array $properties = null) {
		$options = array(
			"" => "--Select State--",
			"ACT" => "ACT",
			"NSW" => "NSW",
			"NT" => "NT",
			"QLD" => "QLD",
			"SA" => "SA",
			"TAS" => "TAS",
			"VIC" => "VIC",
			"WA" => "WA",
			"OTHER" => "Other",
		);
		parent::__construct($label, $name, $options, $properties);
    }
}
